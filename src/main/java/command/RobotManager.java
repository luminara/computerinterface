package command;

import communication.RabbitQueueHelper;
import communication.messages.rx.*;
import communication.messages.tx.MapInitMessage;
import communication.messages.tx.RobotInitMessage;
import robot.Robot;
import robot.factory.RobotFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

//THIS CLASS SHOULD BE ONLY VIA SINGLETON
public abstract class RobotManager<T extends Robot> implements RxMessageVisitor {
    protected Map<String,List<T>> robots = new HashMap<>();//the key is the clientId of the user that has requested the robots
    protected static double l;
    protected static double w;
    protected static double screenY;
    protected static double screenX;


    public RobotManager(double l , double w){
         this.l = l;
         this.w = w;
     }

     //this method must be implemented in all subclasses, since it can't be generalised
    @Override
    public abstract void visit(ControlMessage message);

    @Override
    public void visit(CommunicationInitMessage message) {
        //TODO connect the available robots to the phoneId
        System.out.println("Communication Init: "+message.jsonify().toString());

        screenX = message.getScreenX();
        screenY = message.getScreenY();
        //Store PhoneID
        String phoneId = message.getClientId();
        List<T> available = new ArrayList<>();
        int i=0;
        for (Robot robot : RobotFactory.getRobots()){
            if (i==message.getRobotNumber())
                break;
            if (robot.getStatus() == Robot.RobotStatus.IDLE){
                available.add((T) robot);
                robot.setStatus(Robot.RobotStatus.MOVING);
                i++;
            }

        }

        if ((i+1)!=available.size()){
            //send error message to the user, since there are less available robots than
            // the ones requested
        }

        robots.put(phoneId,available);
        //Send init robot
        RobotInitMessage robotInitMessage = new RobotInitMessage((List<Robot>) available);
        System.out.println("Robot Init: "+ robotInitMessage.jsonify().toString());
        try {
            RabbitQueueHelper.sendMessage(message.getClientId(),robotInitMessage.jsonify().toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        //Send init map
        MapInitMessage mapInitMessage = new MapInitMessage(l,w);
        System.out.println("Map Init: "+ robotInitMessage.jsonify().toString());
        try {
            RabbitQueueHelper.sendMessage(message.getClientId(),mapInitMessage.jsonify().toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Robot> visit(ReleaseRobotMessage message) {
        String clientId = message.getClientId();
        if (robots.containsKey(clientId)){
            List<T> userRobots = robots.get(clientId);
            robots.remove(clientId);
            System.out.println("Release Message: "+message.jsonify());
            return (List<Robot>) userRobots;
        }
        return null;
    }

    //this method must be implemented in all subclasses, since it can't be generalised
    @Override
    public abstract void visit(ControlMessage2 message);

    //Decides the types of message
    public boolean exec(RxAbstractMessage message){
        if (message!=null) {
            message.accept(this);
            return true;
        }
        return false;
    }


    public static class RobotMovement{
        private  double mapWidthInPx = cm2px(w);
        private  double mapLengthInPx = cm2px(l);
        private  double ratioWidth = mapWidthInPx/(screenX);
        private  double ratioLength = mapLengthInPx/(screenY);

        public void move(Robot robot,double x, double y){
            double robotX = robot.getX();
            double robotY = robot.getY();
            int direction = 180;

            double realX = (x);
            double realY = (y);


            double movementX = ((realX)-(robotX));//Delta X
            double movementY = (((realY)-(robotY)));//Delta Y


            double alpha;

            double computedAngle = (robot.rad2degree(Math.atan2(movementY,movementX)));
            double robotAngle = (robot.getVerticalAngle());
            if (computedAngle<0)
                computedAngle=computedAngle+360;

            alpha= (computedAngle-robotAngle)%360;
            robot.setX(realX);
            robot.setY(realY);


            double ipotenusa = Math.sqrt(Math.pow(px2cm(movementX*ratioWidth),2)+Math.pow(px2cm(movementY*ratioLength),2));
            robot.rotate((alpha));

            long time = (long) (timeOfMovement(robot,(ipotenusa))*1000);
            robot.moveForward(time);

        }

        private double timeOfMovement(Robot robot, double space){
            //space in cm
            return space/robot.getRealSpeedUnit();
        }

        //res cm
        private double cm2px(double valCm){
            //proportion
            //1 cm =   37.795276 px
            return valCm*37.795276;
        }

        //res cm
        private double px2cm(double valPx){
            //proportion
            //1 cm =   37.795276 px
            return valPx/37.795276;
        }
    }
}
