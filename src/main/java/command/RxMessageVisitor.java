package command;

import communication.messages.rx.CommunicationInitMessage;
import communication.messages.rx.ControlMessage;
import communication.messages.rx.ControlMessage2;
import communication.messages.rx.ReleaseRobotMessage;
import robot.Robot;

import java.util.List;

public interface RxMessageVisitor{
    public abstract void visit(ControlMessage message);
    public void visit(CommunicationInitMessage message);
    public List<Robot> visit(ReleaseRobotMessage message);
    public abstract void visit(ControlMessage2 message);
}
