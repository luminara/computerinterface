package communication;

public enum Address {
    //"Docker-ExternalLoa-1G2E3VBERIKMD-1846673439.eu-west-1.elb.amazonaws.com"
    RABBITMQ_HOST_ADDRESS("ec2-34-240-40-205.eu-west-1.compute.amazonaws.com"),
    RABBITMQ_PORT("5672"),PC_HOST("PC-STATION");

    private String text;

    private Address(String string){
        text=string;
    }

    @Override
    public String toString() {
        return text;
    }
}
