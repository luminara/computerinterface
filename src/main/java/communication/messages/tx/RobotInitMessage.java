package communication.messages.tx;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import communication.messages.utils.JsonMessageFields;
import communication.messages.utils.MessageType;
import robot.Robot;

import java.util.ArrayList;
import java.util.List;

public class RobotInitMessage extends TxAbstractMessage {
    private List<Robot> robotList=new ArrayList<>();

    public RobotInitMessage(List<Robot> robotList) {
        super(MessageType.INIT_ROBOTS);
        if (robotList!=null)
            this.robotList=robotList;

    }


    @Override
    public JsonObject jsonify() {
        JsonArray robots = JsonObject.ja();
        for (Robot robot : robotList){
            robots.add(robot.jsonify());
        }

        JsonObject jsonObject = super.jsonify().put(JsonMessageFields.ROBOTS.toString(),robots);
        jsonObject.put(JsonMessageFields.ROBOT_NUMBER.toString(),robotList.size());
        return jsonObject;
    }

}
