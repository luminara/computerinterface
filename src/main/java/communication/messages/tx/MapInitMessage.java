package communication.messages.tx;

import com.couchbase.client.java.document.json.JsonObject;
import communication.messages.utils.JsonMessageFields;
import communication.messages.utils.MessageType;

public class MapInitMessage extends TxAbstractMessage {
    private double length = 0;
    private double width = 0;

    public MapInitMessage(double length, double width) {
        super(MessageType.INIT_MAP);
        this.length = length;
        this.width = width;
    }


    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public JsonObject jsonify() {
        JsonObject object = JsonObject.create();
        object.put(JsonMessageFields.LENGTH.toString(),length);
        object.put(JsonMessageFields.WIDTH.toString(),width);
        JsonObject jsonObject = super.jsonify().put(JsonMessageFields.DATA.toString(),object);
        return jsonObject;
    }
}
