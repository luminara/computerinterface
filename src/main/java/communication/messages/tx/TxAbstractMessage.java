package communication.messages.tx;

import com.couchbase.client.java.document.json.JsonObject;
import communication.messages.utils.JsonMessageFields;
import communication.messages.utils.MessageType;

public abstract class TxAbstractMessage {
    private MessageType type = null;

    public TxAbstractMessage(MessageType type){
        this.type=type;
    }

    public MessageType getType() {
        return type;
    }



    public JsonObject jsonify(){
        JsonObject object = JsonObject.create();
        if (type!=null)
            object.put(JsonMessageFields.TYPE.toString(),type.toString());
        return object;
    }
}
