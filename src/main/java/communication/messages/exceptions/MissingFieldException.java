package communication.messages.exceptions;

public class MissingFieldException extends Exception{
    public MissingFieldException(String string) {
        super("Invalid message, missing field: "+string);
    }
}
