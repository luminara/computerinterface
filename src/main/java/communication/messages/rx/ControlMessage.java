package communication.messages.rx;

import com.couchbase.client.java.document.json.JsonObject;
import command.RxMessageVisitor;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.utils.JsonMessageFields;

public class ControlMessage extends RxAbstractMessage {
    private double x;
    private double y;
    private String robotId;
    private String clientId;

    public ControlMessage(String json) throws MissingFieldException {
        super(json);
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            robotId = data.get(JsonMessageFields.ROBOT_ID.toString()).toString();
        }catch (NullPointerException e){
            throw new MissingFieldException("robot id field.");
        }
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            x = Double.parseDouble(data.get(JsonMessageFields.X.toString()).toString());
        }catch(NullPointerException e){
            throw new MissingFieldException("x field.");
        }
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            y = Double.parseDouble(data.get(JsonMessageFields.Y.toString()).toString());
        }catch(NullPointerException e){
            throw new MissingFieldException("y field.");
        }
        try {
            clientId = (JsonObject.fromJson(json).get(JsonMessageFields.PHONE_ID.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("client id field.");
        }
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public String getRobotId() {
        return robotId;
    }

    public String getClientId() { return clientId;
    }

    @Override
    public JsonObject jsonify() {
        JsonObject object = JsonObject.create();
        object.put(JsonMessageFields.X.toString(),x);
        object.put(JsonMessageFields.Y.toString(),y);
        object.put(JsonMessageFields.ROBOT_ID.toString(),robotId);
        JsonObject jsonObject = super.jsonify().put(JsonMessageFields.DATA.toString(),object);
        jsonObject.put(JsonMessageFields.PHONE_ID.toString(),clientId);
        return jsonObject;
    }

    @Override
    public void accept(RxMessageVisitor visitor) {
        visitor.visit(this);
    }
}
