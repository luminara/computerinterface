package communication.messages.rx;

import com.couchbase.client.java.document.json.JsonObject;
import command.RxMessageVisitor;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.utils.JsonMessageFields;

public class ControlMessage2 extends RxAbstractMessage {
    private String movement;
    private String robotId;
    private String clientId;

    public ControlMessage2(String json) throws MissingFieldException {
        super(json);
        try {
            robotId = (JsonObject.fromJson(json).get(JsonMessageFields.ROBOT_ID.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("robot id field.");
        }
        try {
            movement = (JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("data field.");
        }
        try {
            clientId = (JsonObject.fromJson(json).get(JsonMessageFields.PHONE_ID.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("client id field.");
        }
    }


    public String getMovement() {
        return movement;
    }

    public String getRobotId() {
        return robotId;
    }

    public String getClientId() { return clientId;
    }

    @Override
    public JsonObject jsonify() {
        JsonObject jsonObject = super.jsonify().put(JsonMessageFields.DATA.toString(),movement);
        jsonObject.put(JsonMessageFields.PHONE_ID.toString(),clientId);
        jsonObject.put(JsonMessageFields.ROBOT_ID.toString(),robotId);
        return jsonObject;
    }

    @Override
    public void accept(RxMessageVisitor visitor) {
        visitor.visit(this);
    }
}
