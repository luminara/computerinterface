package communication.messages.rx;

import com.couchbase.client.java.document.json.JsonObject;
import command.RxMessageVisitor;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.utils.JsonMessageFields;
import communication.messages.utils.MessageType;

public abstract class RxAbstractMessage{
    private MessageType type = null;

    public RxAbstractMessage(String json) throws MissingFieldException {
        try {
            JsonObject jsonObject = JsonObject.fromJson(json);
            type = MessageType.valueOf(jsonObject.get(JsonMessageFields.TYPE.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("type field.");
        }
    }

    public MessageType getType() {
        return type;
    }

    public static  <T extends RxAbstractMessage> T getMessage(String json) throws MissingFieldException{
        JsonObject jsonObject = JsonObject.fromJson(json);
        MessageType type = MessageType.valueOf(jsonObject.get(JsonMessageFields.TYPE.toString()).toString());

        switch (type){
            case CONTROL:
                return (T) new ControlMessage(json);
            case INIT_COMMUNICATION:
                return (T) new CommunicationInitMessage(json);
            case RELEASE_ROBOTS:
                return (T) new ReleaseRobotMessage(json);
            case CONTROL2:
                return (T) new ControlMessage2(json);
        }
        throw new MissingFieldException("type field.");
    }


    public JsonObject jsonify(){
        JsonObject object = JsonObject.create();
        if (type!=null)
            object.put(JsonMessageFields.TYPE.toString(),type.toString());
        return object;
    }

    public abstract void accept(RxMessageVisitor visitor);
}
