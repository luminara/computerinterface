package communication.messages.rx;

import com.couchbase.client.java.document.json.JsonObject;
import command.RxMessageVisitor;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.utils.JsonMessageFields;

public class ReleaseRobotMessage extends RxAbstractMessage {
    private String clientId;

    public ReleaseRobotMessage(String json) throws MissingFieldException {
        super(json);
        try {
            clientId = (JsonObject.fromJson(json).get(JsonMessageFields.PHONE_ID.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("client id field.");
        }
    }


    public String getClientId() { return clientId;
    }

    @Override
    public JsonObject jsonify() {
        JsonObject jsonObject = super.jsonify();
        jsonObject.put(JsonMessageFields.PHONE_ID.toString(),clientId);
        return jsonObject;
    }

    @Override
    public void accept(RxMessageVisitor visitor) {
        visitor.visit(this);
    }
}
