package communication.messages.rx;

import com.couchbase.client.java.document.json.JsonObject;
import command.RxMessageVisitor;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.utils.JsonMessageFields;

public class CommunicationInitMessage extends RxAbstractMessage {
    private double screenX;
    private double screenY;
    private String clientId;
    private int robotNumber;

    public CommunicationInitMessage(String json) throws MissingFieldException {
        super(json);try {
            clientId = (JsonObject.fromJson(json).get(JsonMessageFields.PHONE_ID.toString()).toString());
        }catch (NullPointerException e){
            throw new MissingFieldException("phone id field.");
        }
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            screenX = Double.parseDouble(data.get(JsonMessageFields.SCREEN_X.toString()).toString());
        }catch(NullPointerException e){
            throw new MissingFieldException("screen x field.");
        }
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            screenY = Double.parseDouble(data.get(JsonMessageFields.SCREEN_Y.toString()).toString());
        }catch(NullPointerException e){
            throw new MissingFieldException("screen y field.");
        }
        try {
            JsonObject data = JsonObject.fromJson(JsonObject.fromJson(json).get(JsonMessageFields.DATA.toString()).toString());
            robotNumber = (int) Double.parseDouble(data.get(JsonMessageFields.ROBOT_NUMBER.toString()).toString());
        }catch(NullPointerException e){
            throw new MissingFieldException("Robot Number field.");
        }
    }

    @Override
    public JsonObject jsonify() {
        JsonObject object = JsonObject.create();
        object.put(JsonMessageFields.SCREEN_X.toString(),screenX);
        object.put(JsonMessageFields.SCREEN_Y.toString(),screenY);
        object.put(JsonMessageFields.ROBOT_NUMBER.toString(),robotNumber);
        JsonObject jsonObject = super.jsonify().put(JsonMessageFields.DATA.toString(),object);
        jsonObject.put(JsonMessageFields.PHONE_ID.toString(),clientId);
        return jsonObject;
    }

    public String getClientId() {
        return clientId;
    }

    public double getScreenX() {
        return screenX;
    }

    public double getScreenY() {
        return screenY;
    }

    public int getRobotNumber() {
        return robotNumber;
    }

    @Override
    public void accept(RxMessageVisitor visitor) {
        visitor.visit(this);
    }
}
