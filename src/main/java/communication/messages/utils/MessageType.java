package communication.messages.utils;

public enum MessageType {
    CONTROL("CONTROL"),CONTROL2("CONTROL2"),INIT_MAP("INIT_MAP"),INIT_COMMUNICATION("INIT_COMMUNICATION"),
    INIT_ROBOTS("INIT_ROBOTS"),RELEASE_ROBOTS("RELEASE_ROBOTS");
    private String text;

    private MessageType(String text){
        this.text=text;
    }


    @Override
    public String toString() {
        return text;
    }
}
