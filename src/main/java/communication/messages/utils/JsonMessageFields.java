package communication.messages.utils;

public enum JsonMessageFields {
    ROBOT_ID("RobotId"),TYPE("Type"),DATA("Data"),
    ROBOTS("Robots"),ROBOT_NUMBER("RobotNumber"),X("X"),Y("Y"),
    SCREEN_X("ScreenX"),SCREEN_Y("ScreenY"),PHONE_ID("PhoneId"),
    LENGTH("Length"),WIDTH("Width"),ROBOT_STATUS("Status");

    private String text;

    private JsonMessageFields(String text){
        this.text=text;
    }


    @Override
    public String toString() {
        return text;
    }
}
