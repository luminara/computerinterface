package communication;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitQueueHelper{

    public static void sendMessage(String queueId, String data) throws IOException, TimeoutException {
        String QUEUE_NAME = queueId;
        Channel channel = ChannelSingleton.getInstance();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, data.getBytes());
    }

    public static void receiveMessage(String queueId,DefaultConsumer consumer) throws IOException, TimeoutException {
        Channel channel = ChannelSingleton.getInstance();
        channel.exchangeDeclare(queueId, "direct");
        channel.queueDeclare(queueId, false, false, false, null);
        channel.queueBind(queueId, queueId, "");
        channel.basicConsume(queueId, true, consumer);
    }



}
