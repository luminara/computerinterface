package robot.elisa.managers;

import command.RobotManager;
import communication.messages.rx.ControlMessage;
import communication.messages.rx.ControlMessage2;
import communication.messages.rx.ReleaseRobotMessage;
import robot.Robot;
import robot.factory.RobotFactory;
import robot.elisa.Elisa3;
import robot.elisa.station.BaseStation;
import robot.elisa.station.BaseStationSingleton;

import java.util.List;

public class Elisa3Manager extends RobotManager<Elisa3>{
    private BaseStation baseStation;
    public Elisa3Manager(double l, double w,int[] addresses) {
        super(l, w);
        //set BaseStation class for elisa3
        //start robots communication (for Elisa3)
        baseStation =  BaseStationSingleton.getInstance();
        if (addresses!=null)
            baseStation.startCommunication(addresses,addresses.length);
        else
            throw new NullPointerException("Addresses array is undefined.");
    }

    public BaseStation getBaseStation() {
        return baseStation;
    }

    @Override
    public List<Robot> visit(ReleaseRobotMessage message) {
        List<Robot> list = super.visit(message);
        if (list!=null) {
            for (Robot robot : list) {
                robot.setStatus(Robot.RobotStatus.IDLE);
                if (robot.getVerticalAngle() < 0)
                    robot.rotate((int) (360 + (robot.getVerticalAngle() % 360)));
                else
                    robot.rotate((int) (360 - Math.abs(robot.getVerticalAngle() % 360)));
                robot.setVerticalAngle(0);
                robot.setX(0);
                robot.setY(0);
            }
        }
        return list;
    }

    @Override
    public void visit(ControlMessage message) {
        //TODO with the values from the message make robots move
        String clientId = message.getClientId();
        String robotId = message.getRobotId();

        System.out.println("Message9: "+ message.jsonify().toString());
        List<Elisa3> availableRobots = robots.get(clientId);
        Robot robot = availableRobots.get(availableRobots.indexOf(RobotFactory.createElisaRobot(robotId,null)));

        //message
        //just testing, so each time a message arrives the robot moves forward
        new RobotMovement().move(robot,message.getX(),message.getY());

    }

    @Override
    public void visit(ControlMessage2 message) {
        //From BigData
        String clientId = message.getClientId();
        String robotId = message.getRobotId();

        System.out.println("Message#Spark: "+ message.jsonify().toString());
        List<Elisa3> availableRobots = robots.get(clientId);
        Robot robot = availableRobots.get(availableRobots.indexOf(RobotFactory.createElisaRobot(robotId,null)));

        switch (message.getMovement()){
            case "forward":
                robot.moveForward(500);
                break;
            case "backward":
                robot.rotate(180);
                break;
            case "right":
                robot.rotate(-90);
                break;
            case "left":
                robot.rotate(90);
                break;
            case "stop":
                robot.stopMovement();
                break;
            /*case "buy":
                if (!(robot.getVerticalAngle()==0)){
                    robot.rotate(-180);
                }
                robot.moveForward(200);
                break;
            case "dump":
                if ((robot.getVerticalAngle()==0)){
                    robot.rotate(180);
                }
                break;
            case "pump":
                if (!(robot.getVerticalAngle()==0)){
                    robot.rotate(-180);
                }
                break;
            case "dead":
                if ((robot.getVerticalAngle()==0)){
                    robot.rotate(180);
                }
                robot.moveForward(200);
                break;
            case "hodl":
                robot.stopMovement();
                break;*/
        }
    }
}
