package robot.elisa.station;/*
Use this wrapper class as a way to be sure
 */

public class BaseStationWrapper implements BaseStation {
    private BaseStation instance = BaseStation.instance;


    @Override
    public void startCommunication(int[] robotAddr, int numRobots) {
        instance.startCommunication(robotAddr, numRobots);
    }

    @Override
    public void stopCommunication() {
        instance.stopCommunication();
    }

    @Override
    public void setLeftSpeed(int robotAddr, short value) {
        instance.setLeftSpeed(robotAddr, value);
    }

    @Override
    public void setRightSpeed(int robotAddr, short value) {
        instance.setRightSpeed(robotAddr, value);
    }

    @Override
    public void setLeftSpeedForAll(byte[] value) {
        instance.setLeftSpeedForAll(value);
    }

    @Override
    public void setRightSpeedForAll(byte[] value) {
        instance.setRightSpeedForAll(value);
    }

    @Override
    public void setRed(int robotAddr, short value) {
        instance.setRed(robotAddr, value);
    }

    @Override
    public void setGreen(int robotAddr, short value) {
        instance.setGreen(robotAddr, value);
    }

    @Override
    public void setBlue(int robotAddr, short value) {
        instance.setBlue(robotAddr, value);
    }

    @Override
    public void setRedForAll(short[] value) {
        instance.setRedForAll(value);
    }

    @Override
    public void setGreenForAll(short[] value) {
        instance.setGreenForAll(value);
    }

    @Override
    public void setBlueForAll(short[] value) {
        instance.setBlueForAll(value);
    }

    @Override
    public void turnOnFrontIRs(int robotAddr) {
        instance.turnOnFrontIRs(robotAddr);
    }

    @Override
    public void turnOffFrontIRs(int robotAddr) {
        instance.turnOffFrontIRs(robotAddr);
    }

    @Override
    public void turnOnBackIR(int robotAddr) {
        instance.turnOnBackIR(robotAddr);
    }

    @Override
    public void turnOffBackIR(int robotAddr) {
        instance.turnOffBackIR(robotAddr);
    }

    @Override
    public void turnOnAllIRs(int robotAddr) {
        instance.turnOnAllIRs(robotAddr);
    }

    @Override
    public void turnOffAllIRs(int robotAddr) {
        instance.turnOffAllIRs(robotAddr);
    }

    @Override
    public void enableTVRemote(int robotAddr) {
        instance.enableTVRemote(robotAddr);
    }

    @Override
    public void disableTVRemote(int robotAddr) {
        instance.disableTVRemote(robotAddr);
    }

    @Override
    public void enableSleep(int robotAddr) {
        instance.enableSleep(robotAddr);
    }

    @Override
    public void disableSleep(int robotAddr) {
        instance.disableSleep(robotAddr);
    }

    @Override
    public void calibrateSensors(int robotAddr) {
        instance.calibrateSensors(robotAddr);
    }

    @Override
    public void calibrateSensorsForAll() {
        instance.calibrateSensorsForAll();
    }

    @Override
    public void enableObstacleAvoidance(int robotAddr) {
        instance.enableObstacleAvoidance(robotAddr);
    }

    @Override
    public void disableObstacleAvoidance(int robotAddr) {
        instance.disableObstacleAvoidance(robotAddr);
    }

    @Override
    public void enableCliffAvoidance(int robotAddr) {
        instance.enableCliffAvoidance(robotAddr);
    }

    @Override
    public void disableCliffAvoidance(int robotAddr) {
        instance.disableCliffAvoidance(robotAddr);
    }

    @Override
    public void setRobotAddress(int robotIndex, int robotAddr) {
        instance.setRobotAddress(robotIndex, robotAddr);
    }

    @Override
    public void setRobotAddresses(int[] robotAddr, int numRobots) {
        instance.setRobotAddresses(robotAddr, numRobots);
    }

    @Override
    public int getProximity(int robotAddr, int proxId) {
        return instance.getProximity(robotAddr, proxId);
    }

    @Override
    public int getProximityAmbient(int robotAddr, int proxId) {
        return instance.getProximityAmbient(robotAddr, proxId);
    }

    @Override
    public int getGround(int robotAddr, int groundId) {
        return instance.getGround(robotAddr, groundId);
    }

    @Override
    public int getGroundAmbient(int robotAddr, int groundId) {
        return instance.getGroundAmbient(robotAddr, groundId);
    }

    @Override
    public void getAllProximity(int robotAddr, int[] proxArr) {
        instance.getAllProximity(robotAddr, proxArr);
    }

    @Override
    public void getAllProximityAmbient(int robotAddr, int[] proxArr) {
        instance.getAllProximityAmbient(robotAddr, proxArr);
    }

    @Override
    public void getAllGround(int robotAddr, int[] groundArr) {
        instance.getAllGround(robotAddr, groundArr);
    }

    @Override
    public void getAllGroundAmbient(int robotAddr, int[] groundArr) {
        instance.getAllGroundAmbient(robotAddr, groundArr);
    }

    @Override
    public void getAllProximityFromAll(int[][] proxArr) {
        instance.getAllProximityFromAll(proxArr);
    }

    @Override
    public void getAllProximityAmbientFromAll(int[][] proxArr) {
        instance.getAllProximityAmbientFromAll(proxArr);
    }

    @Override
    public void getAllGroundFromAll(int[][] groundArr) {
        instance.getAllGroundFromAll(groundArr);
    }

    @Override
    public void getAllGroundAmbientFromAll(int[][] groundArr) {
        instance.getAllGroundAmbientFromAll(groundArr);
    }

    @Override
    public int getBatteryAdc(int robotAddr) {
        return instance.getBatteryAdc(robotAddr);
    }

    @Override
    public int getBatteryPercent(int robotAddr) {
        return instance.getBatteryPercent(robotAddr);
    }

    @Override
    public int getAccX(int robotAddr) {
        return instance.getAccX(robotAddr);
    }

    @Override
    public int getAccY(int robotAddr) {
        return instance.getAccY(robotAddr);
    }

    @Override
    public int getAccZ(int robotAddr) {
        return instance.getAccZ(robotAddr);
    }

    @Override
    public byte getSelector(int robotAddr) {
        return instance.getSelector(robotAddr);
    }

    @Override
    public byte getTVRemoteCommand(int robotAddr) {
        return instance.getTVRemoteCommand(robotAddr);
    }

    @Override
    public void setSmallLed(int robotAddr, int ledId, int state) {
        instance.setSmallLed(robotAddr, ledId, state);
    }

    @Override
    public void turnOffSmallLeds(int robotAddr) {
        instance.turnOffSmallLeds(robotAddr);
    }

    @Override
    public void turnOnSmallLeds(int robotAddr) {
        instance.turnOnSmallLeds(robotAddr);
    }

    @Override
    public int getOdomTheta(int robotAddr) {
        return instance.getOdomTheta(robotAddr);
    }

    @Override
    public int getOdomXpos(int robotAddr) {
        return instance.getOdomXpos(robotAddr);
    }

    @Override
    public int getOdomYpos(int robotAddr) {
        return instance.getOdomYpos(robotAddr);
    }

    @Override
    public int getVerticalAngle(int robotAddr) {
        return instance.getVerticalAngle(robotAddr);
    }

    @Override
    public void startOdometryCalibration(int robotAddr) {
        instance.startOdometryCalibration(robotAddr);
    }

    @Override
    public void transferData() {
        instance.transferData();
    }

    @Override
    public void stopTransferData() {
        instance.stopTransferData();
    }

    @Override
    public void resumeTransferData() {
        instance.resumeTransferData();
    }

    @Override
    public short robotIsCharging(int robotAddr) {
        return instance.robotIsCharging(robotAddr);
    }

    @Override
    public short robotIsCharged(int robotAddr) {
        return instance.robotIsCharged(robotAddr);
    }

    @Override
    public short buttonIsPressed(int robotAddr) {
        return instance.buttonIsPressed(robotAddr);
    }

    @Override
    public void resetFlagTX(int robotAddr) {
        instance.resetFlagTX(robotAddr);
    }

    @Override
    public short getFlagTX(int robotAddr, int flagInd) {
        return instance.getFlagTX(robotAddr, flagInd);
    }

    @Override
    public short getFlagRX(int robotAddr) {
        return instance.getFlagRX(robotAddr);
    }

    @Override
    public long getLeftMotSteps(int robotAddr) {
        return instance.getLeftMotSteps(robotAddr);
    }

    @Override
    public long getRightMotSteps(int robotAddr) {
        return instance.getRightMotSteps(robotAddr);
    }

    @Override
    public double getRFQuality(int robotAddr) {
        return instance.getRFQuality(robotAddr);
    }

    @Override
    public void resetMessageIsSentFlag(int robotAddr) {
        instance.resetMessageIsSentFlag(robotAddr);
    }

    @Override
    public short messageIsSent(int robotAddr) {
        return instance.messageIsSent(robotAddr);
    }

    @Override
    public short waitForMessageTransmission(int robotAddr, long us) {
        return instance.waitForMessageTransmission(robotAddr, us);
    }

    @Override
    public void setCompletePacket(int robotAddr, byte red, byte green, byte blue, byte[] flags, byte left, byte right, byte leds) {
        instance.setCompletePacket(robotAddr, red, green, blue, flags, left, right, leds);
    }

    @Override
    public void setCompletePacketForAll(int[] robotAddr, byte[] red, byte[] green, byte[] blue, byte[][] flags, byte[] left, byte[] right, byte[] leds) {
        instance.setCompletePacketForAll(robotAddr, red, green, blue, flags, left, right, leds);
    }

    @Override
    public short waitForUpdate(int robotAddr, long us) {
        return instance.waitForUpdate(robotAddr, us);
    }

    @Override
    public short sendMessageToRobot(int robotAddr, byte red, byte green, byte blue, byte[] flags, byte left, byte right, byte leds, long us) {
        return instance.sendMessageToRobot(robotAddr, red, green, blue, flags, left, right, leds, us);
    }
}
