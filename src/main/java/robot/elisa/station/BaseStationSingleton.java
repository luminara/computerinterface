package robot.elisa.station;

public class BaseStationSingleton {
    private static BaseStation baseStation = null;

    private BaseStationSingleton(){

    }

    public static BaseStation getInstance(){
        if (baseStation == null){
            baseStation = new BaseStationWrapper();
        }
        return baseStation;
    }
}
