package robot.elisa;

import robot.Robot;
import robot.elisa.station.BaseStation;
import robot.elisa.station.BaseStationSingleton;

public class Elisa3 extends Robot{
    private BaseStation baseStation = BaseStationSingleton.getInstance();
    private int rotationCoefficient = 0;
    private int rotationCoefficient1 = 0;
    private boolean motorsMoving = false;

    public Elisa3(String id) {
        super(id);
        setSpeed((short) 30);

    }

    public Elisa3(String id,RobotOrientation orientation) {
        super(id,orientation);
        setSpeed((short) 30);
    }

    @Override
    public int getBatteryPercent() {
        return baseStation.getBatteryPercent(Integer.parseInt(id));
    }

    public  void turnOnBlueLed(short val){
        baseStation.setBlue(Integer.parseInt(id), (short) val);
    }

    public  void turnOnRedLed(short val){
        baseStation.setRed(Integer.parseInt(id), (short) val);
    }

    public  void turnOnGreenLed(short val){
        baseStation.setGreen(Integer.parseInt(id), (short) val);
    }

    public  void turnOffBlueLed(){
        baseStation.setBlue(Integer.parseInt(id), (short) 0);
    }

    public  void turnOffRedLed(){
        baseStation.setRed(Integer.parseInt(id), (short) 0);
    }

    public  void turnOffGreenLed(){
        baseStation.setGreen(Integer.parseInt(id), (short) 0);
    }

    public void randomColors(){
        double randomVal = Math.abs(Math.random());
        while (randomVal>0.01){
            randomVal = Math.abs(Math.random());
        }
        turnOnBlueLed((short) (randomVal*1000));
        randomVal = Math.abs(Math.random());
        while (randomVal>0.01){
            randomVal = Math.abs(Math.random());
        }
        turnOnRedLed((short) (randomVal*1000));
        randomVal = Math.abs(Math.random());
        while (randomVal>0.01){
            randomVal = Math.abs(Math.random());
        }
        turnOnGreenLed((short) (randomVal*1000));
    }

    @Override
    protected void rotate90Left() {
        rotate(90);
    }

    @Override
    protected void rotate90Right() {
        rotate(-90);
    }

    @Override
    public void turnLeft() {

    }

    @Override
    public void turnRight() {

    }

    @Override
    protected void moveForward() {
        motorsMoving = true;

        baseStation.setLeftSpeed(Integer.parseInt(id), (short) speed);

        baseStation.setRightSpeed(Integer.parseInt(id), (short) (speed));

    }


    @Override
    public void moveForwardFor(double distance) {
        //In CM
        long time = (long) (distance/this.getRealSpeedUnit())*1000;//in millis
        moveForward(time);
    }

    @Override
    public void moveForward(long time) {
        executorService.submit(() -> {
                moveForward();
                //System.out.println("TEST");
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stopMovement();
            });
    }

    @Override
    public void stopMovement() {
        motorsMoving = false;
        baseStation.setLeftSpeed(Integer.parseInt(id), (short) 0);
        baseStation.setRightSpeed(Integer.parseInt(id), (short) 0);
    }

    @Override
    public void moveBackward(long time) {
        executorService.submit(() -> {
            executorService.submit(() -> {
                while (!motorsMoving)
                    moveBackward();
            });
            //System.out.println("TEST");
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stopMovement();
        });
    }

    @Override
    public void moveBackward() {
        motorsMoving = true;
        baseStation.setLeftSpeed(Integer.parseInt(id), (short) -speed);
        baseStation.setRightSpeed(Integer.parseInt(id), (short) -speed);
    }

    @Override
    public double getRealSpeedUnit(){
        //127 = 60cm/s
        //127 : 60 = speed : x
        double realSpeed = (60*speed)/127;
        return realSpeed;
    }

    @Override
    public double getAngularSpeed(){
        //r = 2.5 cm  //omega = v/r
        //2pi : 360 = omega : x
        double omega = getRealSpeedUnit()/2.2;
        return omega;
    }

    @Override
    public void setStatus(RobotStatus status) {
        super.setStatus(status);
        if (status.equals(RobotStatus.IDLE)) {
            turnOffRedLed();
            turnOnBlueLed((short) 100);
            stopMovement();
        }
        else {
            turnOffBlueLed();
            turnOnRedLed((short) 100);
        }
    }

    @Override
    public void rotate(double angle1) {
        //counter clockwise is negative | clockwise is positive
        double angle = angle1;

//        if (angle1>180) {
//            angle1=angle1%180;
//            angle=-(180-angle1);
//        }
//        if (angle1<-180) {
//            angle1=angle1%180;
//            angle=(angle1);
//        }
        double finalAngle = angle;
        currentAngle =  getVerticalAngle()+finalAngle;
        executorService.submit(() -> {
            short tmpSpeed = speed;
            setSpeed((short) 15);

            double time = (getRotationTime(finalAngle))*1000;
            stopMovement();
//            try {
//                Thread.sleep((long) 500);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }


            if (finalAngle >=0) {
                baseStation.setLeftSpeed(Integer.parseInt(id), (short) -speed);
                baseStation.setRightSpeed(Integer.parseInt(id), (short) speed);
            }else {
                baseStation.setLeftSpeed(Integer.parseInt(id), (short) speed);
                baseStation.setRightSpeed(Integer.parseInt(id), (short) -speed);
            }
            try {
                Thread.sleep((long) Math.abs(time));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                stopMovement();
                setSpeed(tmpSpeed);
            }
        });
    }
}
