package robot.factory;


import robot.Robot;
import robot.elisa.Elisa3;

import java.util.ArrayList;
import java.util.List;

//The factory class should be the only one creating instances of robots
//The current factory class doesn't allow for different robot model to be managed at the same time
public class RobotFactory {
    private static List<Robot> robots = new ArrayList<>();

    public static Elisa3 createElisaRobot(String id, Robot.RobotOrientation orientation){
        Elisa3 elisa3;
        if (orientation == null)
            elisa3 = new Elisa3(id);
        else
            elisa3 = new Elisa3(id,orientation);
        if (!robots.contains(elisa3))
            robots.add(elisa3);
        return (Elisa3) robots.get(robots.indexOf(elisa3));
    }

    public static List<Robot> getRobots() {
        return robots;
    }
}
