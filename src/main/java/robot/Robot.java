package robot;

import com.couchbase.client.java.document.json.JsonObject;
import communication.messages.utils.JsonMessageFields;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Robot {
    protected String id;//address
    protected double x=0;
    protected double y=0;
    protected RobotStatus status;
    protected RobotOrientation orientation;
    protected short speed =0;// range -128 to 127
    protected double currentAngle = 0;
    protected ExecutorService executorService = Executors.newSingleThreadExecutor();

    public Robot(String id){
        this.id=id;
        status=RobotStatus.IDLE;
        orientation=RobotOrientation.TOP;
    }

    public Robot(String id,RobotOrientation orientation1){
        this.id=id;
        status=RobotStatus.IDLE;
        orientation=orientation1;
    }

    public String getId() {
        return id;
    }

    public abstract int getBatteryPercent();
    protected abstract void rotate90Left();
    protected abstract void rotate90Right();
    public abstract void turnLeft();
    public abstract void turnRight();
    protected abstract void moveForward();
    public abstract void moveForward(long time);
    public abstract void moveForwardFor(double distance);
    public abstract void moveBackward(long time);
    public abstract void stopMovement();
    protected abstract void moveBackward();
    public void setVerticalAngle(int degrees){
        currentAngle = degrees;
    }
    public double getVerticalAngle(){
        return currentAngle%360;
    }
    public void rotate(double angle){
        currentAngle = angle;
    }

    public void setSpeed(short speed){
        this.speed = speed;
    }

    public void setStatus(RobotStatus status) {
        this.status = status;
    }

    public RobotStatus getStatus() {
        return status;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public short getSpeed() {
        return speed;
    }

    public RobotOrientation getOrientation() {
        return orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof Robot)
                if (id.equals(((Robot) o).id))
                    return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public JsonObject jsonify(){
        JsonObject object = JsonObject.create();
        object.put(JsonMessageFields.ROBOT_ID.toString(),getId());
        JsonObject data = JsonObject.create();
        data.put(JsonMessageFields.X.toString(),getX());
        data.put(JsonMessageFields.Y.toString(),getY());
        data.put(JsonMessageFields.ROBOT_STATUS.toString(),getStatus().toString());
        object.put(JsonMessageFields.DATA.toString(),data);
        return object;
    }

    public abstract double getRealSpeedUnit();

    public abstract double getAngularSpeed();

    private double degree2rad(double degree){
        //degree = Math.abs(degree);
        double rad = (6.28*degree)/360;
        return rad;
    }

    public double rad2degree(double rads){
        //rads = Math.abs(rads);
        double deg = (360*rads)/6.28;
        return deg;
    }

    protected double getRotationTime(double degree){
        //degree = Math.abs(degree);
        double omega = getAngularSpeed();
        double rad = degree2rad(degree);
        return rad/omega;
    }

    public enum RobotStatus {
        MOVING("MOVING"),IDLE("IDLE");

        private String text;

        private RobotStatus(String text){
            this.text=text;
        }


        @Override
        public String toString() {
            return text;
        }
    }

    public enum RobotOrientation {
        TOP("Top"),BOTTOM("Bottom"),LEFT("Left"),RIGHT("Right");

        private String text;

        private RobotOrientation(String text){
            this.text=text;
        }


        @Override
        public String toString() {
            return text;
        }
    }
}
