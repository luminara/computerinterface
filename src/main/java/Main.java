import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import command.RobotManager;
import communication.Address;
import communication.ChannelSingleton;
import communication.RabbitQueueHelper;
import communication.messages.exceptions.MissingFieldException;
import communication.messages.rx.RxAbstractMessage;
import robot.Robot;
import robot.factory.RobotFactory;
import robot.elisa.Elisa3;
import robot.elisa.managers.Elisa3Manager;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Main {
    public static void main(String[] args) throws IOException, TimeoutException {
//       REAL PROGRAM
        ChannelSingleton.setUpConnection();

        int robotNumber = 0;
        int[] addresses;
        double length = 0;
        double width = 0;
        Scanner scanner = new Scanner(System.in);

        //Get inputs from the terminal
        //Get number and address of robots
        System.out.println("Number Of Robots:");
        String input = scanner.nextLine();
        try {
            robotNumber = Integer.parseInt(input);
        }catch (NumberFormatException e){
            System.out.println("User input error "+e.getMessage().toLowerCase()+". Integers only.");
            System.exit(0);
        }

        addresses = new int[robotNumber];

        //TODO Error check the user inputs, also ask user for robot model
        for (int i =0;i<robotNumber;i++){
            System.out.println("Robot #"+i+" address(integer only): ");
            input = scanner.nextLine();
            try {
                addresses[i] = Integer.parseInt(input);
            }catch (NumberFormatException e){
                System.out.println("User input error "+e.getMessage().toLowerCase()+".");
                System.exit(0);
            }
            Robot.RobotOrientation orientation = Robot.RobotOrientation.RIGHT;
            RobotFactory.createElisaRobot(""+addresses[i],orientation);
        }


        //Get Dimensions of the Map
        System.out.println("Map's length(cm): ");
        input = scanner.nextLine();
        length = Double.parseDouble(input);

        System.out.println("Map's width(cm): ");
        input = scanner.nextLine();
        width = Double.parseDouble(input);



        RobotManager<Elisa3> manager = new Elisa3Manager(length,width,addresses);


        RabbitQueueHelper.receiveMessage(Address.PC_HOST.toString(),new DefaultConsumer(ChannelSingleton.getInstance()){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                try {
                    manager.exec(RxAbstractMessage.getMessage(message));

                } catch (MissingFieldException e) {
                    e.printStackTrace();
                }
            }
        });


    if (input.equals("stop")) {
        ChannelSingleton.tearDownConnection();
        System.exit(0);
        //baseStationWrapper.stopCommunication();
    }

    }

}
